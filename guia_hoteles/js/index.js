var btnModal;
$(function() {
    $('[data-toggle="tooltip"]').tooltip()
})
$(function() {
    $('[data-toggle="popover"]').popover()
});
$('#form').on('show.bs.modal', function(e) {
    console.log('Empezando a mostrar el modal');
    // Obteniendo el id del botón que el usuario hizo clic
    btnModal = '#' + e.relatedTarget.id;
    // Modificando clases del botón e inhabilitándolo
    $(btnModal).removeClass('btn-success');
    $(btnModal).addClass('btn-primary');
    $(btnModal).prop('disabled', true);
});
$('#form').on('shown.bs.modal', function(e) {
    console.log('Modal mostrado por completo');
});
$('#form').on('hide.bs.modal', function(e) {
    console.log('Empezando a ocultar el modal');
});
$('#form').on('hidden.bs.modal', function(e) {
    console.log('Modal oculto por completo');
    // Habilitando nuevamente el botón
    $(btnModal).removeClass('btn-primary');
    $(btnModal).addClass('btn-success');
    $(btnModal).prop('disabled', false);
});